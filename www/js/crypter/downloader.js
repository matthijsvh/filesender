/**
 * Download and decrypt data from the server, uses range download when available
 * and falls back to single chunk download.
 *
 * USAGE:
 *
 *  var downloader = new Downloader(chunksize, {
 *  	progress: function( percentage ) { progressbar... },
 *  	onComplete: function(fileStorage) { ... },
 *  	onError: function(error) { ... }
 *  });
 *  downloader.start( url, filename, originalFileSize );
 *
 */
var Downloader = (function() {
    var setStatus = function(statusText, callback) {
        if (typeof callback !== 'function') return;
        callback(statusText);
    },
        setProgress = function(progress, callback) {
            if (typeof callback !== 'function') return;
            callback(progress);
        },
        abortOnError = function(options) {
            if (typeof options.onError === 'function') {
                options.onError('ERROR DOWNLOAD');
                return;
            }
            alert("There was an error attempting to download the file.");
            return;
        }, validateChunk = function(cryptChunk, chunkSize) {
            var final = cryptChunk.charAt(cryptChunk.length - 1);

            if (final != "}") {
                var pos = cryptChunk.indexOf("}");

                if (pos == "-1") {
                    alert("no } in chunk, chunkSize (" + chunkSize + ") too small or corrupted data");
                    return;
                } else {
                    alert("} found at " + pos + "but chunkSize is " + chunkSize);
                    return;
                };
            }
        }, decrypt = function(key, cryptChunk, chunkSize) {
            var base64Chunk = null;

            validateChunk(cryptChunk, chunkSize);

            try {
                base64Chunk = sjcl.decrypt(key, cryptChunk);
            } catch (e) {
                alert("wrong key");
                return null;
            };

            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs
            var byteString = atob(base64Chunk);

            // write the bytes of the string to an ArrayBuffer
            var plainChunk = new ArrayBuffer(byteString.length);

            var ia = new Uint8Array(plainChunk);

            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            };

            return plainChunk;
        };

    /** The downloader object, used for the complete download process
     */
    var Downloader = function(chunkSize, options) {
        // set options for later
        this.options = options;
        this.chunkSize = chunkSize;
        this.xhr = new XMLHttpRequest();
    };

    Downloader.prototype = {
        fileUrl: null,
        useRange: true,
        completed: 0,
        fileStorage: null,

        /**
         * Start downloading from fileUrl. Size is actual byte size of the eventual
         * file, not the encrypted size.
         */
        start: function(fileUrl, size) {
            var that = this,
                key = prompt("Give key used for file encryption", "password");

            setStatus("starting download", this.options.onStatus);

            this.fileUrl = fileUrl;
            this.size = total_cryptlen(size, this.chunkSize);

            this.completed = 0;
            this.fileStorage = new FileStorage();

            this.xhr.addEventListener("load", function(evt) {
                that._requestComplete(evt, key)
            }, false);

            this.xhr.addEventListener("error", function() {
                abortOnError(that.options);
            }, false);

            this.xhr.addEventListener("abort", function() {
                if (typeof that.options.onAbort === 'function') {
                    that.options.onAbort('ABORT DOWNLOAD');
                    return;
                }

                alert("The upload has been canceled by the user or the browser dropped the connection.");
            }, false);


            var fnRready = this.options.onComplete || function() {};

            this.fileStorage.ready = function() {
                if (that.completed < that.size) {
                    if (that.useRange) that._nextDownload();
                    return;
                }

                setProgress(100, that.options.progress);
                fnRready(that.fileStorage);
            };

            this.fileStorage.start(null, size);
            setProgress(0, this.options.progress);
        },

        _encryptedChunkSize: function() {
            return cryptLen(base64Len(this.chunkSize));
        },

        /** called when the file storage is ready for some action
         *
         */
        _writerReady: function() {
            if (this.completed < this.size) {
                if (this.useRange) {
                    this._nextDownload();
                    return;
                }

                this._processSingleRequest();
                return;
            }

            setProgress(100, this.options.progress);
            var fnReady = this.options.onComplete || function() {};

            fnReady(this.fileStorage);
        },

        /** Download the next chunk from the server
         *
         */
        _nextDownload: function() {
            setStatus("downloading chunk", this.options.onStatus);
            var start = this.completed;
            var end = Math.min(this.completed + this._encryptedChunkSize(), this.size);
            this.xhr.open("GET", this.fileUrl);
            this.xhr.setRequestHeader("Range", "bytes=" + start + "-" + (end - 1));
            this.xhr.send();
        },

        /** called when the chunk is completely downloaded
         *
         */
        _requestComplete: function(evt, key) {
            if (evt.target.status == 200) {
                console.log("server doesn't support range request");

                this.useRange = false;

                if (evt.target.response.length != this.size) {
                    alert("unexpected response size, expected " + this.size + ",  received " + evt.target.response.length);
                    return;
                };

                this._processSingleRequest(key, evt.target.response);
                return;
            }
            if (evt.target.status == 206) {
                this.useRange = true;
                var l = cryptLen(base64Len(this.chunkSize));
                /**
                 * TODO this is a valid check for all chunks except the last one.
                 */
                //if (evt.target.response.length != l) {
                //    alert("unexpected response size, expected " + l + ", received " + evt.target.response.length);
                //    return;
                //},

                setStatus("decrypting chunk", this.options.onStatus);

                var plainChunk = decrypt(key, evt.target.response, this.chunkSize);

                if (plainChunk === null) {
                    abortOnError(this.options);
                    return;
                }

                this.completed = Math.min(this.completed + this._encryptedChunkSize(), this.size);
                this.fileStorage.append(plainChunk);

                setProgress(Math.round(this.completed / this.size * 100), this.options.progress);
                return;
            }
            alert("server gave an error (" + evt.target.status + ")");
            return;
        },

        /** Process blob in chunked, used when the HTTP server doesn't support Range
         *
         */
        _processSingleRequest: function(key, crypted) {
            var that = this,
                processSingleChunk = function() {
                    var start = that.completed;
                    var end = Math.min(that.completed + that._encryptedChunkSize(), crypted.length);
                    var cryptChunk = crypted.slice(start, end);

                    setStatus("decrypting chunk", that.options.onStatus);

                    that.completed = end;
                    that.fileStorage.append(decrypt(key, cryptChunk, that.chunkSize));

                    setProgress(Math.round(that.completed / that.size * 100), that.options.progress);
                };

            // overwrite the ready callback, if defined.
            // this way, we keep it local, no need to pass
            // around the key if it can be a closure
            this.fileStorage.ready = function() {
                processSingleChunk(key, crypted);
            };

            processSingleChunk(key, crypted);
        },
    };

    return Downloader;
})();
