<?php
##
## database access settings in php format
## automatically generated from /etc/dbconfig-common/filesender.conf
## by /usr/sbin/dbconfig-generate-include
## Sun, 21 Apr 2013 22:01:58 +0200
##
## by default this file is managed via ucf, so you shouldn't have to
## worry about manual changes being silently discarded.  *however*,
## you'll probably also want to edit the configuration file mentioned
## above too.
##
$dbuser='filesender';
$dbpass='filesender';
$basepath='';
$dbname='filesender';
$dbserver='';
$dbport='';
$dbtype='mysql';
